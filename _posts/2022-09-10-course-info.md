---
layout: post
title: 课程信息
date: 2022-09-10 10:58 +0800
---
课表

|    | 星期一|星期二|星期三|星期四|星期五|
|  ----  |  ----  |
| 上午第一节  | 语文 | 数学 | 英语 | 数学 | 语文 |
| 上午第二节  | 数学 | 语文 | 数学 | 语文 | 英语 |
| 上午第三节  | 科技 | 体育 | 语文 | 体育 | 数学 |
| 下午第一节  | 音乐 | 美术 | 音乐 | 心理防疫 | 美术 |


##  英语
教材PDF版：[下载](https://share.weiyun.com/KqPdic5Y)

唐老师的会议室 **775 992 5339**

第二周作业- [下载](https://share.weiyun.com/587mAcx5)


#### 9月9日
兔子舞： [下载](https://share.weiyun.com/JDJR3lhc)

## 语文
教材PDF版：[下载](https://share.weiyun.com/v6fVxN45)

丁老师的会议室 **577 150 6630**

#### 9月13日
课件下载：[下载](https://share.weiyun.com/StKvXvNf)

## 数学
教材PDF版：[下载](https://share.weiyun.com/8fjM5QoF)

莫老师的会议室 **577 150 6630**

注：数学会议室现在已经不用密码了

#### 9月13日
近似数视频：[下载](https://share.weiyun.com/gAqfh701)
第二周学习单：[下载](https://share.weiyun.com/xNKkskZK)
第三周学习单：[下载](https://share.weiyun.com/qQd3NKSR)

## 科学
教材PDF版：[下载](https://share.weiyun.com/fMUi7qDX)

#### 9月9日
不倒翁视频： [下载](https://share.weiyun.com/6IW8jnJV)

#### 9月13日
临时改成体育课：直播 


## 美术


## 音乐

#### 风吹麦浪
伴奏 [下载](https://share.weiyun.com/XF41g0C5)
范唱 [下载](https://share.weiyun.com/sgeWv8eT)
简谱 [下载](https://share.weiyun.com/rOdFJTcb)

## 体育

会议室编号： **913 887 4964**